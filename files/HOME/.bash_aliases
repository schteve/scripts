### Add current path to root path, e.g. psudo tlmgr (TeXLive) 
# c.f. http://askubuntu.com/a/66697
alias psudo='sudo env PATH="/usr/local/bin:/usr/bin:/bin"'

### Solbuild
alias fetchYml="$HOME/Packaging/common/Scripts/yauto.py"
alias updatePackage='/usr/share/ypkg/yupdate.py'

### libreoffice
alias lalc='libreoffice --calc'

### Singing Exercises
alias ms='musescore $HOME/Documents/MuseScore2/Scores/VocalEx/*'

### ls aliases
alias lll='ls -l'
alias la='ls -a'

# Forgot MATLAB Symbolic Links so to start
# alias matlab='cd ~/Documents/MATLAB /usr/local/MATLAB/R2014a/bin/matlab'
# alias matlabroot='cd /usr/local/MATLAB/R2014a/bin'
