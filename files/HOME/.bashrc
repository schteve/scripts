source /usr/share/defaults/etc/profile

# Add bash aliases.
if [ -f $HOME/.bash_aliases ]; then
    source $HOME/.bash_aliases
fi

# Add to search path
export PATH=$HOME/bin:$PATH

# Powerline
# https://powerline.readthedocs.io/en/master/usage/shell-prompts.html#
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
source /usr/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh
