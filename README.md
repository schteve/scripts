# Scripts
## Maintenance
### Backup
Simple backup script for Linux compatible with supplied desktop shortcut.

Requires: `rsync`

### Firewall
Set up a basic [ufw](https://launchpad.net/ufw) configuration.

Requires: `ufw`

### Reinstall
Configure a newly installed system

### SolUp
Update script for Solus covering eopkgs, snaps, and solbuild, if present.

## Programming
### NewFile
Generates minimal files 

### tabs
Open new gnome-terminal tab in current directory

Requires: `xdotool`, `xprop`

### VMSetup
Configure a newly installed VM

## TEST
Requires: imagination
