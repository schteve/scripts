# Scripts
## Maintenance
### Backup
Simple backup script for Linux compatible with supplied desktop shortcut.
TODO:
- [X] Read from .config/Backup/[...] files
    - Drives lists UUIDS
    - Hosts lists WAN/LAN addresses
- [X] Rotate through predetermined number of folders, like current LANBackup
- [X] Add LAN/WANBackup

WISHLIST:
- [ ] Add nextcloud backup
- [ ] Reintroduce tar/sys

Requires: `rsync`, `tar`

### Firewall
Set up a basic [ufw](https://launchpad.net/ufw) configuration.

Requires: `ufw`

### SolUp
Update script for Solus covering eopkgs, snaps, and solbuild, if present.

## Programming
### NewFile
Generates minimal files 

### tabs
Open new gnome-terminal tab in current directory

Requires: `xdotool`, `xprop`

## TEST
Requires: imagination
