#!/bin/bash
# This is tabs, a script to manage gnome-terminal tabs for Solus
#
# Copyright © 2017-2018 Steven Moseley <stevenmos@kolabnow.com>
#
# tabs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published 
# by the Free Software Foundation; version 3 of the License, or 
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# Set up firewall 

MET="tabs"
VER=0.1.1

usage()
{
    echo -e "$MET v$VER Copyright © 2017-2018 Steven Moseley
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to 
redistribute it under certain conditions; see 
GPL-3.0 at <http://www.gnu.org/licenses/gpl-3.0.html>.
\nUsage: $MET                Opens new tab in same directory as current tab 
    -m | --main [file.cc]          Makefile and ./src/file
    -hd | --header [filei.h]       ./src/file.h and /src/file.cc
    -a | --all [file.cc]           ./Makefile and ./src/file, and src/*.h & 
                              src*.cc nicely in vim
"
}

argexist(){
    #
    # @argexist() $1 $2  where $1 == '[var name]', $2 new value for var
    #
    if [ "$2" != "" ]; then
        local var1="$1"
        eval $var1=$2
    else 
        echo "No argument"
        exit
    fi
    shift; shift
    return
}

# https://stackoverflow.com/questions/1188959/open-a-new-tab-in-gnome-terminal-using-command-line

opentab(){
    WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)" | awk '{print $5}')
    xdotool windowfocus $WID
    xdotool key ctrl+shift+t
    #wmctrl -i -a $WID

    sleep 0.25; 
    xdotool type --delay 1 --clearmodifiers "cd $CURR";
    xdotool key Return; 
}

commandintab(){
    sleep 0.25; 
    xdotool type --delay 1 --clearmodifiers "$*";
    xdotool key Return; 
    sleep 0.75; 
}
 
main=""
CURR=$(env | grep "^PWD=" | cut -c 5- -)

while [ "$1" != "" ]; do
    case $1 in
        -h | --help )
            usage
            exit
            ;;
        -m | --main )
            argexist 'main' $2
            echo $main

            opentab
            commandintab vim -O Makefile ./src/$main 
            exit
            ;;
        -hd | --header )
            argexist 'main' $2
            echo $main

            CC=$(echo $main | sed 's/.h$/.cc/g')

            opentab
            commandintab vim -O ./src/$main ./src/$CC
            exit
            ;;
        -a | --all)
            argexist 'main' $2
            echo $main

            opentab
            commandintab vim -O Makefile ./src/$main

            HDR=$(echo $(ls src/*.h))
            
            for x in $HDR; do 
                CC=$(echo $x | sed 's/.h$/.cc/g')
                opentab
                commandintab vim -O $x $CC
            done

            exit
            ;;
        * )                    	
            ;;
    esac
    shift
done

opentab
